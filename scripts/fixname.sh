#!/bin/sh
if [ -f $1 ] ; then
    output="$(echo "$1" | iconv -cf UTF-8 -t ASCII//TRANSLIT | tr ' ' '_')"
    mv $1 $output
else
    echo "'$1' is not a valid file"
fi
