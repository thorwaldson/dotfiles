;; Turn off mouse interface
(when window-system
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (tooltip-mode -1))

;; Set initial Buffer
(setq initial-scratch-message "#+title: Scratch Buffer\n\n"
      initial-major-mode 'org-mode)

;; Set up package
(require 'package)
(setq-default load-prefer-newer t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("elpa" . "https://elpa.gnu.org/packages/") t)
(package-initialize)


;; use-package for ease of instalation
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package)
  )

;; from use-package readme
(eval-when-compile
  (require 'use-package))
(require 'bind-key)

(use-package org)
;; load the config
(org-babel-load-file (concat user-emacs-directory "config.org"))
