export QT_QPA_PLATFORMTHEME="qt5ct"
export EDITOR=/usr/bin/vim
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
if [ -f "$HOME/applications/LibreWolf-84.0.2-2.x86_64.AppImage" ]; then
    export BROWSER=$HOME/applications/LibreWolf-84.0.2-2.x86_64.AppImage
elif [ -f "/sbin/librewolf" ] ; then
    export BROWSER=/sbin/librewolf
fi
if [ -f "/usr/local/bin/st" ] ; then
    export TERMINAL=/usr/local/bin/st
elif [ -f "/sbin/st" ] ; then
    export TERMINAL=/sbin/st
fi
export LOCATION=25813
export QT_STYLE_OVERRIDE=kvantum

if [ -d "/bin" ] ; then
    PATH="/bin:$PATH"
fi
if [ -d "$HOME/bin" ] ; then
	PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.gem/ruby/2.7.0/bin" ] ; then
	PATH="$HOME/.gem/ruby/2.7.0/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ] ; then
	PATH="$HOME/.local/bin:$PATH"
fi
if [ -d "$HOME/.local/bin/statusbar" ] ; then
    PATH="$HOME/.local/bin/statusbar:$PATH"
fi
if [ -d "$HOME/.local/bin/cron" ] ; then
    PATH="$HOME/.local/bin/cron:$PATH"
fi
if [ -d "$HOME/tor/tor-browser_en-US" ] ; then
    PATH="$HOME/tor/tor-browser_en-US:$PATH"
fi
if [ -d "/usr/share/applications" ] ; then
    PATH="/usr/share/applications:$PATH"
fi
if [ -d "/usr/share/applications/kde4" ] ; then
    PATH="/usr/share/applications/kde4:$PATH"
fi
if [ -d "/var/lib/flatpak/exports/share/applications" ] ; then
    PATH="/var/lib/flatpak/exports/share/applications:$PATH"
fi
if [ -d "$HOME/.local/bin/appimage" ] ; then
	PATH="$HOME/.local/bin/appimage:$PATH"
fi
if [ -d "$HOME/applications" ] ; then
    PATH="$HOME/applications:$PATH"
fi
if [ -d "$HOME/applications/activitywatch" ] ; then
    PATH="$HOME/applications/activitywatch:$PATH"
fi
if [ -d "$HOME/.cargo/bin" ] ; then
    PATH="$HOME/.cargo/bin:$PATH"
fi
if [ -d "$HOME/dotfiles/scripts" ] ; then
    PATH="$HOME/dotfiles/scripts:$PATH"
fi
if [ -d "$HIOME/dotfiles/scripts/cron" ] ; then
    PATH="$HOME/dotfiles/scripts/cron:$PATH"
fi
if [ -d "/usr/bin" ] ; then
    PATH="/usr/bin:$PATH"
fi
if [ -d "$HOME/.poetry/bin" ] ; then
	PATH="$HOME/.poetry/bin:$PATH"
fi
if [ -d "$HOME/applications/arm/gcc-arm-none-eabi-10-2020-q4-major/bin" ] ; then
    PATH="$HOME/applications/arm/gcc-arm-none-eabi-10-2020-q4-major/bin:$PATH"
fi

export QT_STYLE_OVERRIDE=kvantum
export GITUSER=Thorwaldson
export TODODIR=$HOME/todo
export NOTESDIR=$HOME/notes
export LANG=en_GB.UTF-8
export LANGUAGE=en_GB
export LC_CTYPE=de_DE.UTF-8
export LC_NUMERIC=de_DE.UTF-8
export LC_TIME=de_DE.UTF-8
export LC_COLLATE=de_DE.UTF-8
export LC_MONETARY=de_DE.UTF-8
export LC_MESSAGES=en_GB.UTF-8
export LC_PAPER=de_DE.UTF-8
export LC_NAME=de_DE.UTF-8
export LC_ADDRESS=de_DE.UTF-8
export LC_TELEPHONE=de_DE.UTF-8
export LC_MEASUREMENT=de_DE.UTF-8
export LC_IDENTIFICATION=de_DE.UTF-8
alias neofetch="neofetch --source ~/dotfiles/config/custom-neofetch.txt"
